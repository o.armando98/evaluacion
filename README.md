# Evaluación

Crear un proyecto básico en el lenguaje que el equipo decida. Este podría ser una pagina web, un microservicio, algún script, etc ...

Cada equipo deberá trabajar en el repositorio asignado. Este repositorio lo podrán encontrar dentro del grupo de [Repositorios](https://gitlab.com/e967)


### Estructura del repositorio 
Configurar el repositorio asignado de tal forma que cumpla con las siguientes caracteristicas:

**Tabla 1.** Miembros y Roles.

| Miembro     | Rol | 
|:-------:    |:--------:  |          
| Miembro 1   | Maintainer |
| Miembro 2   | Developer |
| Miembro 3   | Developer |
| Miembro 4 (Si aplica)   | Developer |

**Tabla 2.** Ramas estables.

| Branch     | Protected | 
|:-------:    |:--------:  |          
| Master   | Yes |

**Tabla 3.** Ramas Temporales.

- Cada miembro del equipo de crear su propia rama temporal siguiendo el **Naming Convention**. El nombre de la rama debe contener el nombre del autor de esta.

| Branch     | Protected | 
|:-------:    |:--------:  |          
| Miembro 1   | No |
| Miembro 2   | No |
| Miembro 3   | No |
| Miembro 4 (Si aplica)   | No |

- Por lo que el repositorio deberá contener la estructura de ramas mostrada en la figura 1.

  &nbsp;&nbsp;&nbsp;_ _ _ _ _ _ _ _ _ *Miembro1* <br>
 / _ _ _ _ _ _ _ _ _ _ _ *Miembro2* <br>
| _ _ _ _ _ _ _ _ _ _ _ _ *master* **Protected** <br>
 \ &nbsp; _ _ _ _ _ _ _ _ _ *Miembro N.* <br>

**Figura 1.** Modelo de ramas.

### Puntos a evaluar

- Que el repositorio cumpla con la estructura propuesta.
- Que el proyecto tenga su *gitgnore*.
- Que los commits realizados en el proyecto cumplan con el estandar de **Conventional Commits**
- Que al menos cada uno de los miembros haya hecho un *revert* en su rama.
- Que las ramas de los miembros se hayan fusionado a la rama master así como entre ellas. 
- Que cada uno de los integrantes haya creado un Tag anotado.

